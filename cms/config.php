<?php
    ini_set( "display_errors", true );
    date_default_timezone_set( "Europe/Kiev" );  
    define( "DB_DSN", "localhost;dbname=db_citadel" );
    define( "DB_USERNAME", "Alex" );
    define( "DB_PASSWORD", "12345" );
    define( "CLASS_PATH", "classes" );
    define( "TEMPLATE_PATH", "templates" );
    define( "HOMEPAGE_NUM_ARTICLES", 5 );
    define( "ADMIN_USERNAME", "admin" );
    define( "ADMIN_PASSWORD", "12345" );
    require( CLASS_PATH . "/Article.php" );

    function handleException( $exception ) {
    echo "Sorry, a problem occurred. Please try later.";
    error_log( $exception->getMessage() );
    }

    set_exception_handler( 'handleException' );
?>
