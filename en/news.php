<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>News</title>
    <!-- Jqury -->
    <script src="./../bower_components/jquery/jquery.min.js"></script>
    <!-- Bootstrap components -->
    <link href="./../bower_components/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./../bower_components/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="./../bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- CSS -->
    <link href="./../dist/css/main.css" rel="stylesheet" />
    <link href="./../bower_components/icomoon/style-fonts.css" rel="stylesheet" />
    <!-- scroll style -->
    <link type="text/css" rel="stylesheet" href="./../bower_components/scroll/jquery.jscrollpane.css"/>
    <!-- scroll script -->
    <script type="text/javascript" src="./../bower_components/scroll/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="./../bower_components/scroll/jquery.jscrollpane.js"></script>
</head>


<body>
    <!-- header -->
    <header>
        <nav class="navbar navbar-default citadel-nav-menu">
            <div class="container-fluid">
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./index.php">CITADEL</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="./news.php">News</a></li>
                        <li><a href="#">Gallery</a></li>
                        <li><a href="./table.php">Timetable</a></li>
                        <li><a href="./reviews.php">Reviews</a></li>
                        <li>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                    Select language
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li id="ua">
                                        <a href="../html/home-page/index.php">
                                            <img src="../../img/flag_ukraine.png" title="українська" alt="українська">
                                            українська
                                        </a>
                                    </li>
                                    <li id="ru">
                                        <a href="../ru/index.php">  
                                            <img src="../../img/flag_russia.png" title="русский" alt="русский">
                                            русский
                                        </a>    
                                    </li>
                                    <li id="en">
                                        <a href="./index.php">
                                            <img src="../../img/flag_great_britain.png" title="english" alt="english">
                                            english
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- header/END -->
    <!-- registration-block -->
    <div class="reg-block clearfix">
        <div class="container-fluid">
    <?php
    if (!isset($_SESSION['login']) or $_SESSION['login']=='') {
        print <<<HERE
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="enter-btn">Enter</a></li>
                <li><a href="#" class="registration-btn">Registered</a></li>
            </ul>            

HERE;
    } else {
        print <<<HERE
            <img alt='$_SESSION[login]' class="user-avatar" src='$_SESSION[avatar]'> 
            <span class="user-name">$_SESSION[login]</span>
            <ul class="nav navbar-nav navbar-right">
                <li><a href='./../../exit.php' class="exit-btn">Exit</a></li>
            </ul>            
HERE;
    }
?>
        </div>
    </div>
    <!-- registration-block -->
    <!-- autorization/registration -->
    <div id="background-autorization">
        <div class="container-fluid">
            <div class="enter" id='enter-form'>
                <div class="row">
                    <form action="../../autho.php" method="POST"  class="col-md-6 col-md-offset-3">
                        <div class="clearfix">
                            <label for="user-name">Enter your name or email</label>
                            <input type="text" class="form-control" id="user-name" placeholder="user name" name="user-name" required>
                            <label for="user-password">Enter your password</label>
                            <input type="password" class="form-control" id="user-password" placeholder="password" name="user-password" required>
                            <input type="submit" class="btn btn-primary btn-lg" value="ENTER" name="submit">
                        </div>
                        <div class="forgot-password">
                            <a href="#">Forgot password?</a>
                        </div>
                        </div>
                    </form>
                </div>
                <div class="registration" id="registration-form">
                    <div class="row">
                        <form action="../../registration.php" method="POST" class="col-md-6 col-md-offset-3" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="user-name">Enter your name</label>
                                <input type="text" class="form-control" id="user-name" placeholder="user name" name="user-name" required>
                                <label for="email">Enter your email</label>
                                <input type="email" class="form-control" id="email" placeholder="email" name="email" required>
                                <label for="avatar">Select a picture</label>
                                <input type="FILE" id="avatar" value="file" name="fupload">
                                <label for="user-password">Enter password</label>
                                <input type="password" class="form-control" id="user-password" placeholder="password" name="user-password" required>
                                <label for="repeat-password"Repeat password</label>
                                <input type="password" class="form-control" id="repeat-password" placeholder="repeat-password" name="repeat-password" required>
                                <input type="submit" class="btn btn-primary btn-lg" value="REGISTERED" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- autorization/registration/END -->
    <!-- select.news -->
    <section class="news">
        <div class="container">
            <div class="row">
                <article class="col-md-8 scroll-pane" id="news-section"></article>
            </div>
        </div>
    </section>
    <!-- select.news/END -->
    <!-- footer -->
    <footer class="clearfix">
        <div class="container">
            <span>
               &copy by Oleksiy Polonka
            </span>
            <ul>
                <li><a href="https://www.facebook.com/groups/485294151639860/?ref=bookmarks" target="_blank">
                    <span class="icon-facebook2"></span>
                    </a>
                </li>
                <li><a href="https://vk.com/krb_cv" target="_blank">
                        <span class="icon-vk"></span>
                    </a>
                </li>
            </ul>
        </div>
    </footer>
    <!-- footer/END -->
    <!-- React js -->
    <script src="./../bower_components/react/react.js"></script>
    <script src="./../bower_components/react/react-dom.js"></script>
    <!-- script -->
    <script src="./../dist/js/main.js"></script></body>
</body>
</html>