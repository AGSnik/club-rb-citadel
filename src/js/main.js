$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: "../../cms/news.php",
        success: function(data) {
            data.map(function(news) {
                $('.scroll-pane').append('<div class="news__block">'+
                    '<h3 class="newsHeader">'+
                        news.title
                    +'</h3>' +
                    '<p class="news__text">'+
                        news.summary
                    +'...</p>'+
                    '<div class="clearfix">'+
                        '<span class="news__data">'+
                            news.publicationDate
                        +'</span>'+
                        '<a href="../news/news-content.php?id='+news.id+'">Читать дальше</a>'
                    +'</div>'
                +'</div>'
                )
            })
        }
    })
    
    $('.news-comments').ready(function() {
        $.ajax({
            method: "GET",
            url: "../../cms/news.php",
            success: function(data) {
                data.map(function(news) {
                    $(this).append('<div class="news__block">'+
                        '<h3 class="newsHeader">'+
                            news.title
                        +'</h3>' +
                        '<p class="news__text">'+
                            news.summary
                        +'...</p>'+
                        '<div class="clearfix">'+
                            '<span class="news__data">'+
                                news.publicationDate
                            +'</span>'+
                            '<a href="../news/news-content.php?id='+news.id+'">Читать дальше</a>'
                        +'</div>'
                    +'</div>'
                    )
                })
            }
        })
    })
    

    $('.enter-btn').click(function () {
            $('#enter-form').css('display', 'block');
            $('#registration-form').css('display', 'none');
            $('#background-autorization').slideToggle();
    });

    $('.registration-btn').click(function () {
        $('#registration-form').css('display', 'block');
        $('#enter-form').css('display', 'none');
        $('#background-autorization').slideToggle();
    });

})