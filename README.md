# Premier Bet Mobile
### Version
0.1.0
# Getting Started

#### 1. Install npm:

[node.js](https://nodejs.org/en/)

#### 2. Install Gulp globally:

__If you have previously installed a version of gulp globally, please run `npm rm --global gulp`
to make sure your old version doesn't collide with gulp-cli.__

```sh
$ npm install --global gulp-cli
```

#### 3. Install Gulp in this project devDependencies:

```sh
$ npm install --save-dev gulp
```

#### 4. Install the following Gulp plugins:

```sh
$ npm install --save-dev gulp-notify gulp-sass gulp-minify-css gulp-autoprefixer 
gulp-rename gulp-uglyfly gulp-babel babel-preset-es2015 gulp-livereload gulp-connect

```

#### 5. Use command to run:

```sh
$ gulp
```