<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="ua">

<head>
    <meta charset="UTF-8">
    <title>Цитадель</title>
    <!-- Jquery -->
    <script src="../../bower_components/jquery/jquery.min.js"></script>
    <!-- Bootstrap components -->
    <link href="../../bower_components/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../bower_components/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="../../bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- CSS -->
    <link href="../../dist/css/main.css" rel="stylesheet" />
    <link href="../../bower_components/icomoon/style-fonts.css" rel="stylesheet" />
    <!-- scroll style -->
    <link type="text/css" rel="stylesheet" href="../../bower_components/scroll/jquery.jscrollpane.css"/>
    <!-- scroll script -->
    <script type="text/javascript" src="../../bower_components/scroll/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="../../bower_components/scroll/jquery.jscrollpane.js"></script>
</head>

<body>
    <!-- header -->
    <header>
        <nav class="navbar navbar-default citadel-nav-menu">
            <div class="container-fluid">
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./index.php">ЦИТАДЕЛЬ</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../news/news.php">Новини</a></li>
                        <li><a href="#">Галерея</a></li>
                        <li><a href="../table/table.php">Розклад</a></li>
                        <li><a href="../reviews/reviews.php">Відгуки</a></li>
                        <li>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                    Виберіть мову
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li id="ua">
                                        <a href="./index.php">
                                            <img src="../../img/flag_ukraine.png" title="українська" alt="українська">
                                            українська
                                        </a>
                                    </li>
                                    <li id="ru">
                                        <a href="../../ru/index.php">  
                                            <img src="../../img/flag_russia.png" title="русский" alt="русский">
                                            русский
                                        </a>    
                                    </li>
                                    <li id="en">
                                        <a href="../../en/index.php">
                                            <img src="../../img/flag_great_britain.png" title="english" alt="english">
                                            english
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- header/END -->
    <!-- registration-block -->
    <div class="reg-block clearfix">
        <div class="container-fluid">
<?php
    if (!isset($_SESSION['login']) or $_SESSION['login']=='') {
        print <<<HERE
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="enter-btn">Увійти</a></li>
                <li><a href="#" class="registration-btn">Зареєструватись</a></li>
            </ul>            

HERE;
    } else {
        print <<<HERE
            <img alt='$_SESSION[login]' class="user-avatar" src='$_SESSION[avatar]'> 
            <span class="user-name">$_SESSION[login]</span>
            <ul class="nav navbar-nav navbar-right">
                <li><a href='./../../exit.php' class="exit-btn">Вийти</a></li>
            </ul>            
HERE;
    }
?>
        </div>
    </div>
    <!-- registration-block -->
    <!-- autorization/registration -->
    <div id="background-autorization">
        <div class="container-fluid">
            <div class="enter" id='enter-form'>
                <div class="row">
                    <form action="../../autho.php" method="POST"  class="col-md-6 col-md-offset-3">
                        <div class="clearfix">
                            <label for="user-name">Введіть ім'я або email</label>
                            <input type="text" class="form-control" id="user-name" placeholder="user name" name="user-name" required>
                            <label for="user-password">Введіть пароль</label>
                            <input type="password" class="form-control" id="user-password" placeholder="password" name="user-password" required>
                            <input type="submit" class="btn btn-primary btn-lg" value="УВІЙТИ" name="submit">
                        </div>
                        <div class="forgot-password">
                            <a href="#">Забули пароль?</a>
                        </div>
                        </div>
                    </form>
                </div>
                <div class="registration" id="registration-form">
                    <div class="row">
                        <form action="../../registration.php" method="POST" class="col-md-6 col-md-offset-3" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="user-name">Введіть ім'я</label>
                                <input type="text" class="form-control" id="user-name" placeholder="user name" name="user-name" required>
                                <label for="email">Введіть email</label>
                                <input type="email" class="form-control" id="email" placeholder="email" name="email" required>
                                <label for="avatar">Виберіть фото</label>
                                <input type="FILE" id="avatar" value="file" name="fupload">
                                <label for="user-password">Введіть пароль</label>
                                <input type="password" class="form-control" id="user-password" placeholder="password" name="user-password" required>
                                <label for="repeat-password">Повторіть пароль</label>
                                <input type="password" class="form-control" id="repeat-password" placeholder="repeat-password" name="repeat-password" required>
                                <input type="submit" class="btn btn-primary btn-lg" value="ЗАРЕЄСТРУВАТИСЬ" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- autorization/registration/END -->
    <!-- carousel -->
    <section class="news-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div id="in-play-carousel" class="carousel slide premier-carousel" data-ride="carousel">
                        <ol class="carousel-indicators premier-carousel-indicators">
                            <li data-target="#in-play-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#in-play-carousel" data-slide-to="1"></li>
                            <li data-target="#in-play-carousel" data-slide-to="2"></li>
                            <li data-target="#in-play-carousel" data-slide-to="3"></li>
                        </ol>
                        <div class="carousel-inner premier-carousel-inner">
                            <div class="item carousels-item active">
                                <img src="../../img/citadel.jpg" alt="fourth slide">
                            </div>
                            <div class="item carousels-item">
                                <img src="../../img/crossfitM.jpg" alt="third slide">
                            </div>
                            <div class="item carousels-item">
                                <img src="../../img/crossfitW.jpg" alt="second slide">
                            </div>
                            <div class="item carousels-item">
                                <img src="../../img/info.jpg" alt="first Slide">
                            </div>
                        </div>
                        <a class="carousel-control left" href="#in-play-carousel" data-slide="prev">
                            <span class="glyphicon glyphicon-menu-left glyphicon-l"></span>
                        </a>
                        <a class="carousel-control right" href="#in-play-carousel" data-slide="next">
                            <span class="glyphicon glyphicon-menu-right glyphicon-r"></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="scroll-pane" id="news-summary"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- carousel/End --> 
    <!-- section.about-us -->
    <section class="gym">
        <div class="container">
            <div class="text-center title">
                <h2>Про Клуб</h2>
            </div>
            <div class="gym-rb gym-padding clearfix">
                <img class="small-image" src="../../img/gym-rb.jpg" alt="спорт-зал">
                <div class="gym-info">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni non,
                    fuga cupiditate blanditiis adipisci id animi perferendis quis, et
                    debitis nobis quasi amet obcaecati deleniti itaque, iusto fugiat 
                    vero velit delectus sapiente! Molestias ad laudantium repellendus,
                    culpa ex quidem, modi voluptates magnam dolorum eveniet aperiam
                    distinctio, natus quas tempora quia? Eum dolor repellat tempora
                    adipisci, culpa fugiat! Exercitationem laudantium quisquam, ad 
                    assumenda quas? Non expedita sit eius, incidunt vitae similique 
                    possimus illum, veniam quisquam, quos tenetur? Illo quaerat voluptatem vero?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni non,
                    fuga cupiditate blanditiis adipisci id animi perferendis quis, et
                    debitis nobis quasi amet obcaecati deleniti itaque, iusto fugiat 
                    vero velit delectus sapiente! Molestias ad laudantium repellendus,
                    culpa ex quidem, modi voluptates magnam dolorum eveniet aperiam
                    distinctio, natus quas tempora quia? Eum dolor repellat tempora
                    adipisci, culpa fugiat! Exercitationem laudantium quisquam, ad 
                    assumenda quas? Non expedita sit eius, incidunt vitae similique 
                    possimus illum, veniam quisquam, quos tenetur? Illo quaerat voluptatem vero?
                </div>
            </div>
            <div class="gym-crossfit gym-padding clearfix">
                <img class="small-image" src="../../img/cross-fit.jpg" alt="зал-crossfit">
                <div class="gym-info">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni non,
                    fuga cupiditate blanditiis adipisci id animi perferendis quis, et
                    debitis nobis quasi amet obcaecati deleniti itaque, iusto fugiat 
                    vero velit delectus sapiente! Molestias ad laudantium repellendus,
                    culpa ex quidem, modi voluptates magnam dolorum eveniet aperiam
                    distinctio, natus quas tempora quia? Eum dolor repellat tempora
                    adipisci, culpa fugiat! Exercitationem laudantium quisquam, ad 
                    assumenda quas? Non expedita sit eius, incidunt vitae similique 
                    possimus illum, veniam quisquam, quos tenetur? Illo quaerat voluptatem vero?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni non,
                    fuga cupiditate blanditiis adipisci id animi perferendis quis, et
                    debitis nobis quasi amet obcaecati deleniti itaque, iusto fugiat 
                    vero velit delectus sapiente! Molestias ad laudantium repellendus,
                    culpa ex quidem, modi voluptates magnam dolorum eveniet aperiam
                    distinctio, natus quas tempora quia? Eum dolor repellat tempora
                    adipisci, culpa fugiat! Exercitationem laudantium quisquam, ad 
                    assumenda quas? Non expedita sit eius, incidunt vitae similique 
                    possimus illum, veniam quisquam, quos tenetur? Illo quaerat voluptatem vero?
                </div>
            </div>
        </div>
    </section>   
    <!-- section.about-us/END --> 
    <div class="map">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-1">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2333.9615270075305!2d25.921770715238527!3d48.27342714966903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473408b3d4e94ddb%3A0xae564c8fa317f952!2z0LLRg9C7LiDQn9Cw0LLQu9CwINCa0LDRgdC_0YDRg9C60LAsIDU0LCDQp9C10YDQvdGW0LLRhtGWLCDQp9C10YDQvdGW0LLQtdGG0YzQutCwINC-0LHQu9Cw0YHRgtGM!5e1!3m2!1suk!2sua!4v1467045819634" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>               
                </div>
                <div class="col-md-4">
                    <p class="text-bold-grey">Адреса:</p>
                    м. Чернівці, вул. Каспрука, 8 ( Канівська,50)
                    гуртожиток ЧПК, вхід зі сторони стадіону.
                    <p class="text-bold-grey">тел.для довідок:</p>
                    <p>067-371-46-31</p>
                    <p>050-683-10-73</p>
                </div>
            </div>
        </div>
    </div> 
    <footer class="clearfix">
        <div class="container">
            <span>
               &copy by Oleksiy Polonka
            </span>
            <ul>
                <li><a href="https://www.facebook.com/groups/485294151639860/?ref=bookmarks" target="_blank">
                    <span class="icon-facebook2"></span>
                    </a>
                </li>
                <li><a href="https://vk.com/krb_cv" target="_blank">
                        <span class="icon-vk"></span>
                    </a>
                </li>
            </ul>
        </div>
    </footer>
    <!-- script -->
    <script src="../../dist/js/main.js"></script></body>
</body>
</html>